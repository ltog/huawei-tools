$modems = get-wmiobject -class "Win32_POTSModem" # https://gallery.technet.microsoft.com/scriptcenter/aca5c056-a9e1-4311-a7d5-f626c5eedaad

$status = "Unknown"

foreach ($modem in $modems) {
	if (!($modem.Status.compareTo("OK"))) {
		Write-Host "Attached To: " $modem.AttachedTo
		$comPort = $modem.AttachedTo
		$comPortNr = $comPort.Substring(3)
		Write-host $comPort
		ttpmacro.exe D:\PROJEKTE\USB-Internet-Dongle_Scripting\set_linux.ttl $comPortNr
		$status = "Done"
	}
}

if(!($status.compareTo("Done"))) {
	Write-Host "Success"
} else {
	Write-Host "Failed"
	$wait = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyUp")
}

